<?php
require_once("$_SERVER[DOCUMENT_ROOT]/classes/simple_html_dom.php");
require_once("$_SERVER[DOCUMENT_ROOT]/classes/Connect.php");
require_once("$_SERVER[DOCUMENT_ROOT]/classes/Data.php");

function e($d, $exit=true){
	echo '<pre>';
	if(is_array($d) || is_object($d)) print_r($d);
	else echo $d;
	if($exit) exit('</pre>');
	echo('</pre>');
}

function get_web_page($url){
	$user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36';

	$options = array(
//		CURLOPT_PROXY          =>'200.75.51.148:8080',
		CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
		CURLOPT_POST           =>false,        //set to GET
		CURLOPT_USERAGENT      => $user_agent, //set user agent
		CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
		CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
		CURLOPT_RETURNTRANSFER => true,     // return web page
		CURLOPT_HEADER         => true,    // don't return headers
		CURLOPT_FOLLOWLOCATION => true,     // follow redirects
		CURLOPT_ENCODING       => "",       // handle all encodings
		CURLOPT_AUTOREFERER    => true,     // set referer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
		CURLOPT_TIMEOUT        => 120,      // timeout on response
		CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
	);

	$ch      = curl_init( $url );
	curl_setopt_array( $ch, $options );
	$content = curl_exec( $ch );
	$err     = curl_errno( $ch );
	$errmsg  = curl_error( $ch );
	$header  = curl_getinfo( $ch );
	curl_close( $ch );

	$header['errno']   = $err;
	$header['errmsg']  = $errmsg;
	$header['content'] = $content;
	return $header;
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

function return_json($status, $data=[]){
	exit(json_encode([
		'status' => $status,
		'data' => $data
	]));
}

function have($value, $required=false){
	return isset($value) && !empty($value) ? $value : ($required ? e('Required parameter does not exists') : false);
}

function create_dir($path){
	$l_path = '';
	foreach(explode('/',$path) as $dir){
		if($dir){
			$l_dir = server_dir("$l_path/$dir");
			if(!file_exists($l_dir)) mkdir($l_dir, 0777);
			$l_path .= "/$dir";
		}
	}
}

@define(EXPORT_DIR, "$_SERVER[DOCUMENT_ROOT]/exports/");

@define(DB_HOST, "localhost");
@define(DB_NAME, "atracker");
@define(DB_USER, "root");
@define(DB_PASS, "");


//@define(DB_HOST, "31.131.27.237");
//@define(DB_NAME, "admin_amazonparser");
//@define(DB_USER, "admin_amp");
//@define(DB_PASS, "M0yiXdgr8Q");
