<?php
include 'settings.php';
include 'test.php';

$Data = new Data();

// Add product
if(isset($_POST['name']) && !empty($_POST['ASIN'])){
    $n = trim($_POST['name']);
    $A = trim($_POST['ASIN']);

    if($Data->table('products')->where('asin', $ASIN)->count()){
	    echo "<div class='orange'>Current ASIN already exists.</div>";
    }else{
        if($Data->table('products')->insert([
	        'name' => $n,
	        'asin' => $A,
        ])) echo '<div class="green">Successfully added</div>';
        else echo '<div class="orange">Oops! Some errors. Product did not added</div>';
    }
}


// Add Group
if(!empty($_POST['group-products']) && isset($_POST['group-name'])){
	$check = new Data();
    $check->table('products');

    $d_ASINs = explode(',', $_POST['group-products']);
	foreach ($d_ASINs as $k=>&$item){
		preg_match('/\((.+)\)/', $item, $t);
		$item = trim($t[1]);
		$k ? $check->orWhere('asin', $item) : $check->where('asin', $item);
	}

	$n = trim($_POST['group-name']) ?: implode(',',$d_ASINs);

	try{
		if($check->count() != count($d_ASINs)) throw new Exception('Invalid ASINs');
        if((new Data)->table('groups')->insert([
	        'name' => $n,
            'asins' => implode(',', $d_ASINs),
            'tag' => time()
        ])){
	        echo '<div class="green">Successfully added</div>';
        }else{
            throw new Exception('DB error');
        }
    }catch (Exception $e){
	    echo "<b class='orange'>Data did not saved (".$e->getMessage().")</b>";
    }
}

// Get products
$products = $Data->table('products')->orderBy('name')->get();
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Amazon track</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="/plugins/tagify.css">
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/plugins/tagify.min.js"></script>

<div class="block">
    <div class="split-2">
        <h1>Products:</h1>

        <button id="setup" onclick="$('#addform').toggle(200)">Add product</button>

        <div style="display:none" id="addform">
            <form action="/" method="POST">
                <input type="text" name="ASIN" placeholder="ASIN" required>
                <input type="text" name="name" placeholder="Name (only for you)">
                <button type="submit" id="save">Add</button>
            </form>
        </div>

        <ul id="list">
		    <?php foreach ($products as $product):?>
                <li><a class="product-link" href="info.php?asin=<?php echo $product['asin']?>"><?php echo "$product[name] ($product[asin])"?></a></li>
		    <?php endforeach;?>
        </ul>

    </div>

    <div class="split-2">
        <h1>Groups:</h1>

        <button id="setup_group" onclick="$('#addgroup').toggle(200)">Add group</button>

        <div style="display:none" id="addgroup">
            <form action="/" method="POST">
                <input type="text" name="group-products" id="tags" placeholder="Enter products name or ASIN here and press ENTER...">
                <input type="text" name="group-name" placeholder="Name (only for you)">
                <button type="submit" id="save">Add</button>
            </form>
        </div>

        <ul id="list">
		    <?php foreach ($Data->table('groups')->orderBy('name')->get() as $group):?>
                <li><a class="product-link" href="info.php?group=<?php echo $group['tag']?>"><?php echo "$group[name]"?></a></li>
		    <?php endforeach;?>
        </ul>

    </div>
</div>

<script>
    $(document).ready(function() {

        var tagify2 = new Tagify(document.getElementById('tags'), {
            enforeWhitelist: true,
            suggestionsMinChars : 1,
            duplicates : false,
            whitelist: [<?php foreach ($products as $product) echo "'$product[name]($product[asin])',"?>]
        });
    });
</script>

</body>
</html>