<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";
include 'test.php';

$ASIN = addslashes(have($_GET['asin']));
$keyword = addslashes(have($_GET['keyword'], true));

$Data = new Data();
$title = '';

$data = [];
if($ASIN){
	$product = $Data->table('products')->where('asin', $ASIN)->one();
	$title = $product['name'];
	$data[$ASIN] = $Data->table('statistics')->where('asin', $ASIN)->where('keyword', $keyword)->where('type', 'product')->get();
}elseif ( $group = intval(have($_GET['group'], true)) ){
	$group = $Data->table('groups')->where('tag', $group)->one();
	$title = $group['name'];

	foreach (explode(",", $group['asins']) as $ASIN){
		$data[$ASIN] = $Data->table('statistics')->where('asin', $ASIN)->where('keyword', $keyword)->where('type', 'group')->get();
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSV TRACK</title>
    <link rel="stylesheet" href="plugins/jquery.dataTables.min.css">
    <link rel="stylesheet" href="plugins/datepicker.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div class="block">
    <h1>Details of <?php echo $title?> <small><?php echo $ASIN ? " $ASIN" : $group['asins']?></small></h1>
</div>
<div class="block">
    <div id="chartContainer" style="width: 100%; height: 500px;"></div>
</div>
<div class="block">
    <button id="back" onclick="history.back()">Back</button>
</div>

<script>
    $(document).ready(function () {
        var data = <?php echo json_encode($data)?>,
            $chartContainer = $("#chartContainer"),
            chart_data = [];

        $.each(data, function(asin, dates){
            var points = [];
            $.each(dates, function(date, item){
                points.push({
                    x: new Date(date),
                    y: parseInt(item.page)
                });
            });

            chart_data.push({
                type: 'line',
                legendText: asin,
                showInLegend: true,
                dataPoints: points
            });
        });

        $chartContainer.CanvasJSChart({
            zoomEnabled: true,
            animationEnabled: true,
            axisX: {
                labelFontSize: 16
            },
            axisY: {
                labelFontSize: 16
            },
            data: chart_data
        });

    });
</script>

</body>
</html>