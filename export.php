<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";

$type = 'product';
if(!empty($_GET['asin']) || !empty($_GET['group'])){
	$ASINs = [addslashes($_GET['asin'])];

	if(isset($_GET['group'])){
		$type = 'group';
		$group = (new Data)->table('groups')->where('tag', intval($_GET['group']))->one();
		$ASINs = explode(',', $group['asins']);
	}
}else exit('Invalid URL');

$csv = "ASIN,Keywords,Position,Page,Date,Status\n";
foreach ($ASINs as $ASIN){
	$timestamp = have((new Data())->table('statistics')->orderBy('DESC', 'tag')->select('tag')->where('type', $type)->where('asin', $ASIN)->one()['tag']);

	$Data = new Data();
	$data = $Data->table('statistics')->orderBy('date')->where('asin', $ASIN)->where('tag', $timestamp)->get();

	foreach ($data as $item){
		$csv .= "$ASIN,$item[keyword],". (intval($item['position']) > 0 ? $item['position'] : '-') .",". (intval($item['page']) > 0 ? $item['page'] : '20+') .",$item[date],$item[status]\n";
	}
}

header('Content-Disposition: attachment; filename="'.date('d.m.y.H.i.s') . '.csv";');
exit($csv);