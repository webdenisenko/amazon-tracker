<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";
include "$_SERVER[DOCUMENT_ROOT]/classes/Tracking.php";

$ASIN = addslashes(have($_POST['ASIN']));
$type = 'product';

if(!$ASIN){
	$ASIN = addslashes(have($_POST['group'], true));
	$type = 'group';
}

$timestamp = (string) time();

$Data = new Data();

$data = null;
if($type == 'product'){
	$data = $Data->table('products')->where('asin', $ASIN)->one();
}else{
	$data = $Data->table('groups')->where('tag', $ASIN)->one();
}

$data = have($data, true);
$keywords = explode("\n", $data['keywords']);

$result = [];

if($type == 'product'){
	$Tracking = new Tracking($ASIN, $timestamp, $type);
	$result = $Tracking->track($keywords);
}else{
	foreach (explode(',', $data['asins']) as $ASIN){
		$Tracking = new Tracking($ASIN, $timestamp, $type);
		$result[$ASIN] = $Tracking->track($keywords);
	}
}

return_json('success', $result);