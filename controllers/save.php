<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";

$data = have($_POST['data'], true);
$ASIN = have($_POST['ASIN']);
$saved = false;

if($ASIN){
	$saved = (new Data())->table('products')->where('asin', addslashes($ASIN))->edit([
		'keywords'=>$data
	]);
}else{
	$group = have($_POST['group'], true);
	$saved = (new Data())->table('groups')->where('tag', intval($group))->edit([
		'keywords'=>$data
	]);
}

return_json($saved ? 'success' : 'error');