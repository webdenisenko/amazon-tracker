<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";

$ASIN = have($_POST['ASIN']);
$timestamp = (new Data())->table('statistics')->where('asin', $ASIN)->orderBy('DESC', 'tag')->select('tag');

$q_data = (new Data())->table('statistics');
if($ASIN){
	$timestamp = $timestamp->where('type', 'product');
	$q_data->where('asin', $ASIN)->where('type', 'product');
}else{
	$group = have($_POST['group'], true);
	$timestamp = $timestamp->where('type', 'group');
	$q_data->where('type', 'group');
}

$timestamp = have($timestamp->one()['tag']);
$q_data = $q_data->where('tag', $timestamp)->orderBy('date')->get();
$data = [];
$statistics = [];

if($ASIN){
	$data = $q_data;

	$to = strtotime("midnight");
	$from = $to - 2592000;

	$dates = (new Data())->table('avg')->where( 'asin', $ASIN )->date('date', date('Y-m-d', $from), date('Y-m-d', $to))->get();
	foreach ($dates as $item){
		$statistics[$ASIN][date('Y.m.d', strtotime($item['date']))] = [
			'position' => $item['position'],
			'page' => $item['page']
		];
	}
}else{
	foreach ($q_data as $item){
		if(!isset($data[$item['keyword']])) $data[$item['keyword']] = [];
		$data[$item['keyword']][$item['asin']] = $item;

		$to = strtotime("midnight");
		$from = $to - 2592000;

		$dates = (new Data())->table('avg')->where( 'asin', $item['asin'] )->date('date', date('Y-m-d', $from), date('Y-m-d', $to))->get();

		foreach ($dates as $item){
			$statistics[$item['asin']][date('Y.m.d', strtotime($item['date']))] = [
				'position' => $item['position'],
				'page' => $item['page']
			];
		}

	}
}

return_json('success', [
	'items' => $data,
	'statistics' => $statistics,
	'timestamp' => $timestamp
]);