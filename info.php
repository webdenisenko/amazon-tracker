<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";
include 'test.php';

$ASIN = addslashes(have($_GET['asin']));

$Data = new Data();
$title = '';
$keywords = '';

if($ASIN){
	$product = $Data->table('products')->where('asin', $ASIN)->one();
	$title = $product['name'];
	$keywords = $product['keywords'];
}elseif ( $group = intval(have($_GET['group'], true)) ){
	$group = $Data->table('groups')->where('tag', $group)->one();
	$title = $group['name'];
	$keywords = $group['keywords'];
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSV TRACK</title>
    <link rel="stylesheet" href="plugins/jquery.dataTables.min.css">
    <link rel="stylesheet" href="plugins/datepicker.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<audio id="success_audio">
    <source src="audio/notify.ogg" type="audio/ogg">
    <source src="audio/notify.mp3" type="audio/mpeg">
    <source src="audio/notify.m4r" type="audio/m4r">
</audio>

<audio id="error_audio">
    <source src="audio/error.ogg" type="audio/ogg">
    <source src="audio/erorr.mp3" type="audio/mpeg">
    <source src="audio/erorr.m4r" type="audio/m4r">
</audio>

<div class="block">
    <h1>Track of <?php echo $title?> <small><?php echo $ASIN ? " $ASIN" : $group['asins']?></small></h1>
</div>
<div class="block">
    <div id="chartContainer" style="width: 100%; height: 500px;"></div>
</div>
<div class="block">
    <button id="back" onclick="location.href = '/'">Back</button>
    <button id="update">Update</button>
    <button id="setup">Setup keywords</button>
    <button onclick="location.href = '/export.php?<?php echo $ASIN ? "asin=$ASIN" : "group=$group[tag]"?>'" style="float:right" id="export">Export(csv)</button>
    <span id="status"><b class="gray">Loaidng...</b></span>
</div>
<div class="block" style="display:none" id="keywords">
    <button id="save">Save</button>
    <span id="save_status"></span>
    <textarea id="keywords_textarea" placeholder="One keywords to one line..."><?php echo $keywords?></textarea>
</div>
<div class="block">
    <table id="list" class="display" cellspacing="0" width="100%"></table>
</div>

<script>
    $(document).ready(function () {

        var notify_allow = Notification.permission.toLowerCase();

        switch (notify_allow) {
            case "denied":
                alert('Внимание! Вы не включили уведомления в браузере.')
                break;

            case "default":
                Notification.requestPermission(function (permission) {
                    if (permission != "granted") return false;
                    new Notification("Great! I am test message :)");
                });
        }

        function show_message(title, text, icon, tag) {
            return notify_allow == 'granted' ? new Notification(title, {
                tag: tag ? tag : 'default',
                body: text,
                icon: icon
            }) : alert(title + '\n' + text);
        }

        var data = [],
            update_interval = null,
            counts = {
                all: 0,
                success: 0,
                errors: 0,
                unprocessed: 0,
            },
            timestamp = 0,
            $update = $('#update'),
            $setup = $('#setup'),
            $save = $('#save'),
            $list = $('#list').DataTable({
                scrollY:        "300px",
                scrollX:        true,
                scrollCollapse: true,
                fixedColumns:   {
                    leftColumns: 2
                },
                data: data,
                columns: [
                    {
                        title: "#",
                        data: "id"
                    },
                    {
                        title: "Keyword",
                        data: "keyword",
                        render: function(val){
                            return "<a href='/single.php?<?php echo $ASIN ? "asin=$ASIN&" : "group=$group[tag]&"?>keyword="+val+"'>"+val+"</a>";
                        }
                    },
                    {
                        title: "Position<?php echo $ASIN ? '' : " ($group[asins])"?>",
                        data: "position",
                        render: function(val){
                            return val == 0 ? '-' : val;
                        }
                    },
                    {
                        title: "Page<?php echo $ASIN ? '' : " ($group[asins])"?>",
                        data: "page",
                        render: function(val){
                            return val == 0 ? '20+' : val;
                        }
                    },

                ],
                columnDefs: [
                    {
                        targets: [ 0, 1, 2 ],
                        className: 'mdl-data-table__cell--non-numeric'
                    }
                ],
                iDisplayLength: 25
            }),
            $keywords_textarea = $('#keywords_textarea'),
            $chartContainer = $("#chartContainer");

        function monitoring() {
            $('#success_audio')[0].play();
            update_interval = setInterval(function(){
                update_list();
            }, 15000);
            show_message('Tracking!', 'It may take more than 15 minutes. You can refresh the page to monitoring status...');
        }

        function update_list(){
            // Send ajax query
            $.ajax({
                url: 'controllers/get.php',
                type: 'POST',
                data: {
                    <?php echo $ASIN ? "'ASIN': '$ASIN'" : "group:'$group[tag]'"?>
                },
                dataType: 'json',
                complete: function (res) {
                    if (res && res.responseJSON) {
                        res = res.responseJSON;

                        if (res.status == 'success') {
                            res = res.data;

                            data = res.items;

                            var chart_data = [];

                            counts = {
                                all: <?php echo $ASIN ? 'data.length' : '0'?>,
                                success: 0,
                                errors: 0,
                                unprocessed: 0,
                            }

                            $list.clear();

                            <?php if($ASIN):?>
                            $.each(data, function(k, item) {
                                switch (item.status) {
                                    case 'success':
                                        counts.success++;
                                        break;
                                    case 'error':
                                        counts.error++;
                                        break;
                                    case 'unprocessed':
                                        counts.unprocessed++;
                                        break;
                                }
                            });
                            <?php else:?>
                            var d = data;
                            data = [];

                            var names = <?php
	                        $names = [];
	                        foreach (explode(',', $group['asins']) as $ASIN_item){
		                        $names[$ASIN_item] = $Data->table('products')->where('asin', $ASIN_item)->one()['name'];
                            }
                            echo json_encode($names);
                            ?>

                            var id,pos,page;
                            $.each(d, function(keyword, product) {
                                id = '';
                                pos = '';
                                page = '';

                                $.each(product, function(asin, item) {
                                    counts.all++;

                                    id += item.id;

                                    if(item.position == '0') item.position = '-';
                                    if(item.page == '0') item.page = '20+';

                                    pos += (pos?' / ':'')+'<span class="group-value" title="'+names[item.asin]+'">'+item.position+'</span>';
                                    page += (page?' / ':'')+'<span class="group-value" title="'+names[item.asin]+'">'+item.page+'</span>';

                                    switch (item.status) {
                                        case 'success':
                                            counts.success++;
                                            break;
                                        case 'error':
                                            counts.error++;
                                            break;
                                        case 'unprocessed':
                                            counts.unprocessed++;
                                            break;
                                    }
                                });
                                data.push({
                                    'id': id,
                                    'keyword': keyword,
                                    'position': pos,
                                    'page': page
                                });
                            });
                            <?php endif;?>

                            $list.rows.add(data);
                            $list.draw();

                            $.each(res.statistics, function(asin, dates){
                                var points = [];
                                $.each(dates, function(date, item){
                                    points.push({
                                        x: new Date(date),
                                        y: parseInt(item.page)
                                    });
                                });

                                chart_data.push({
                                    type: 'line',
                                    legendText: asin,
                                    showInLegend: true,
                                    dataPoints: points
                                });
                            });

                            if(data.length){
                                $chartContainer.show(500).CanvasJSChart({
                                    zoomEnabled: true,
                                    animationEnabled: true,
                                    axisX: {
                                        labelFontSize: 16
                                    },
                                    axisY: {
                                        labelFontSize: 16
                                    },
                                    data: chart_data
                                });
                            }else{
                                $chartContainer.hide(500);
                            }

                            timestamp = res.timestamp;
                            update_status();

                            return true;
                        }
                    }

                    $('#error_audio')[0].play();
                    show_message('Oops!', 'Something went wrong.');
                    console.error('ERROR:', res);
                }
            });
        }update_list();

        function track() {
            // Send ajax query
            $.ajax({
                url: 'controllers/list.php',
                type: 'POST',
                data: {
	                <?php echo $ASIN ? "'ASIN': '$ASIN'" : "group:'$group[tag]'"?>
                },
                dataType: 'json',
                beforeSend: function () {
                    $update.prop('disabled', true);
                    show_message('Procesings is started!', 'Don\'t refresh the page.');
                },
                complete: function (res) {
                    if (res && res.responseJSON) {
                        res = res.responseJSON;

                        if (res.status == 'success') {
                            res = res.data;
                            $update.prop('disabled', false);
                            return monitoring();
                        }
                    }

                    $('#error_audio')[0].play();
                    show_message('Oops!', 'Something went wrong.');
                    console.error('ERROR:', res);
                }
            });
        }

        function update_counter() {
            var count = $keywords_textarea.val() ? $keywords_textarea.val().split('\n').length : 0;
            $('#save_status').html(count + ' keywords');
        }update_counter();

        function update_status(){
            var status = '<b class="gray">Data is not set yet</b>';

            if(counts.all){
                if(!counts.unprocessed){
                    var date = new Date(timestamp*1000);
                    date = (date.getMonth() + 1) + '.' + date.getDate() + '.' +  date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
                    status = "<b class='green'>Last update: "+date+"</b>";
                    clearInterval(update_interval);
                }else{
                    status = "<b class='orange'>Update not complete yet... ("+counts.success+" of "+counts.all+")</b>";
                }

                if(counts.errors) status += " <small class='red'>We have "+counts.errors+" errors.</small>";
            }

            $('#status').html(status);
        }

        $update.click(function(e){
            e.preventDefault();
            track();
        });

        $setup.click(function(e){
            e.preventDefault();
            $('#keywords').toggle(200);
        });

        $keywords_textarea.on('keypress, blur', function(){
            update_counter();
        });

        $save.click(function(e){
            e.preventDefault();

            $.ajax({
                url: 'controllers/save.php',
                type: 'POST',
                data: {
	                <?php echo $ASIN ? "'ASIN': '$ASIN'" : "group:'$group[tag]'"?>,
                    'data': $keywords_textarea.val()
                },
                dataType: 'json',
                complete: function (res) {
                    if(res && res.responseJSON){
                        res = res.responseJSON;

                        if(res.status == 'success'){
                            $('#success_audio')[0].play();
                            $('#keywords').hide(200);
                            show_message('Saved!', 'All keywords saved. Please, update logs.');
                            return update_counter();
                        }
                    }

                    $('#error_audio')[0].play();
                    show_message('Oops!', 'Something went wrong.');
                    console.error('ERROR:', res);
                }
            });
        });
    });
</script>

</body>
</html>