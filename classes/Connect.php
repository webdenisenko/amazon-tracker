<?php
class Connect{
    public function connection() {
        $mysqli = @new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if (mysqli_connect_errno()) {
            printf("Connection error: %s\n", mysqli_connect_error());
            exit();
        }

        $mysqli->query ('SET NAMES utf8');
        $mysqli->query ('SET CHARACTER SET utf8');

        return $mysqli;
    }
}