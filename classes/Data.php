<?php

class Data{
    public
        $table,
        $col='*',
        $where,
        $date,
        $last_month,
        $like,
        $order,
        $limit;

    private $mysqli;

    public function __construct(){
        $connection = new Connect();
        $this->mysqli = $connection->connection();
    }

    public function table($value){
        $this->table='';
        $this->col='*';
        $this->where='';
        $this->like='';
        $this->order='';
        $this->limit='';

        $this->table = $value;
        return $this;
    }

    public function where($p,$a,$v=null){
        if($v === null){
            $v = $a;
            $a = '=';
        }

        $this->where .= ($this->where?' AND ':'')."`{$p}`$a'{$v}'";

        return $this;
    }

    public function orWhere($p,$a,$v=null){
        if($v === null){
            $v = $a;
            $a = '=';
        }
        $this->where .= ' OR '."`{$p}`$a'{$v}'";

        return $this;
    }

    public function like($p,$v){
        $this->like .= ($this->like?' AND ':$this->where?' AND ':'')."{$p} LIKE '{$v}'";

        return $this;
    }

    public function orLike($p,$v){
        $this->like .= ($this->like?' OR ':$this->where?' OR ':'')."{$p} LIKE '{$v}'";

        return $this;
    }

    public function orderBy($t,$v=null){
        if(!$v) $v = 'id';

        $t = mb_strtoupper($t);
        if(!in_array($t,['DESC','ASC'])) $t = '';

        $val = "`$v` $t";
        if(gettype($v) == 'array'){
            $val = '';
            foreach($v as $c) $val .= ($val?',':'')."`$c` $t";
        }

        $this->order = "ORDER BY $val";
        return $this;
    }

    public function limit($v){
        $this->limit = "LIMIT $v";
        return $this;
    }

    public function select(){
        $this->col = '';

        foreach(func_get_args() as $c){
            $val = gettype($c) == 'array' ? $c : [$c];
            foreach($val as $v) $this->col .= ($this->col?',':'')."`$v`";
        }

        if($this->col === '') $this->col = '*';

        return $this;
    }

    public function avg(){
        $this->col = '';

        foreach(func_get_args() as $c){
            $val = gettype($c) == 'array' ? $c : [$c];
            foreach($val as $v) $this->col .= ($this->col?',':'')."avg($v)";
        }

        if($this->col === '') $this->col = 'avg(*)';

        return $this;
    }

    public function date($col, $d1, $d2){
	    $this->date = "`$col` BETWEEN '$d1' AND '$d2'";

	    return $this;
    }

    public function month($col){
    	$this->last_month = "`$col`";
	    return have($this->musqli_query());
    }

    public function insert(array $data){
        $ins_p = '';
        $ins_v = '';

        foreach($data as $key=>$value){
            $ins_p .= ($ins_p?',':'')."`$key`";
            $ins_v .= ($ins_v?',':'')."'{$value}'";
        }

        $inserted = $this->mysqli->query("INSERT INTO `$this->table` ($ins_p) VALUES($ins_v)");
        return $inserted ? $this->mysqli->insert_id : false;
    }

    public function edit($data, $update_date=''){
        $q_set = '';
        foreach($data as $key=>$value) $q_set .= ($q_set?',':'')."`$key`='{$value}'";
        if($update_date){
        	$q_set .= ($q_set?',':'')."`$update_date`=current_timestamp";
        }

        $query = "UPDATE `{$this->table}` SET $q_set";

        if($this->where) $query .= " WHERE $this->where";
        if($this->limit) $query .= " $this->limit";

        $data_query = $this->mysqli->query($query);

        return $data_query;
    }

    public function one(){
        $this->limit(1);
        return have($this->musqli_query()[0]) ?: [];
    }

    public function get(){
        return $this->musqli_query();
    }

    public function pag($cur_page=1,$per_page=10){;
        $count_rows = $this->count();
        $count_pages = ceil($count_rows / $per_page);

        if($cur_page > $count_pages) $cur_page = $count_pages;
        if($cur_page < 1) $cur_page = 1;

        $start = ($cur_page - 1) * $per_page;

        return [
            'count'=>intval($count_rows),
            'pages'=>intval($count_pages),
            'current'=>intval($cur_page),
            'data'=>$this->limit("$start,$per_page")->musqli_query()
        ];
    }

    public function count(){
        $count = $this->mysqli->query("SELECT count(*) FROM `$this->table`".(($this->where || $this->like)?' WHERE ':'').$this->where.$this->like);

        return $count ? $count->fetch_array()[0] : 0;
    }

    public function remove(){
        $query = "DELETE FROM `{$this->table}`";
        if($this->where || $this->like){
            $query .= " WHERE ";
            if($this->where) $query .= $this->where;
            if($this->like) $query .= $this->like;
        }
        if($this->order) $query .= " $this->order";
        if($this->limit) $query .= " $this->limit";

        $data_query = $this->mysqli->query($query);

        return $data_query;
    }

    public function query($q){
        return $this->mysqli->query($q);
    }

    private function musqli_query(){
        $data_array = [];

        $query = "SELECT {$this->col} FROM `{$this->table}`";
        if($this->where || $this->like){
            $query .= " WHERE ";
	        if($this->date) $query .= "($this->date)";
            if($this->last_month) $query .= ($this->date?' AND ':'')."($this->last_month > DATE_SUB(NOW(), INTERVAL 1 MONTH))";
            if($this->where) $query .= (($this->date||$this->last_month)?' AND ':'').$this->where;
            if($this->like) $query .= $this->like;
        }
        if($this->order) $query .= " $this->order";
        if($this->limit) $query .= " $this->limit";

        $data_query = $this->mysqli->query($query);

        if ($data_query) {
            while ($item = $data_query->fetch_assoc()) array_push($data_array, $item);
        }

        return count($data_array) ? $data_array : [];
    }
}