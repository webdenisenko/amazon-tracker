<?php
include 'Threads.php';

class Tracking {
	private $ASIN = '';
	private $tag = '';
	private $type = '';

	function __construct($ASIN, $tag, $type) {
		$this->ASIN = $ASIN;
		$this->tag = $tag;
		$this->type = $type;
	}

	static $depth = 20;
	static $stream_length = 15;

	function error($keyword, $errno, $http_code){
		$Data = new Data();
		return $Data->table('errors')->insert([
			'asin' => trim($this->ASIN),
			'keyword' => trim($keyword),
			'errno' => addslashes($errno),
			'http_code' => intval($http_code),
			'tag' => $this->tag,
		]);
	}

	function log($keyword_id, $position, $page, $status='success'){
		$Data = new Data();
		return $Data->table('statistics')->where('id', $keyword_id)->edit([
			'position' => intval($position),
			'page' => intval($page),
			'type' => $this->type,
			'status' => $status,
		], 'date');
	}

	function avg(){
		if((new Data)->table('statistics')->where('type', $this->ASIN)->where('tag', 1501477516)->where('status', 'unprocessed')->count()) return false;
		$curr_date = (new Data())->table('statistics')->where('tag', $this->tag)->avg('page', 'position')->one();

		$position = ceil($curr_date['avg(position)']);
		$page = ceil($curr_date['avg(page)']);

		if(!$position) return false;

		$data = [
			'asin' => $this->ASIN,
			'position' => $position,
			'page' => $page,
			'date' => date('Y.m.d', strtotime("midnight"))
		];

		if((new Data)->table('avg')->where('asin', $this->ASIN)->where('date', $data['date'])->count()){
			return (new Data)->table('avg')->where('asin', $this->ASIN)->where('date', $data['date'])->edit($data);
		}else{
			return (new Data)->table('avg')->insert($data);
		}
	}

	function check($keywords){

		for ($i=0; $i<count($keywords); $i++){
			$keyword = (object) $keywords[$i];

			if(!$keyword->id) continue;

			$position = 0;
			$added = false;
			$error = false;

			for ($d=1; $d < $this::$depth+1; $d++){
				$page = get_web_page( "https://www.amazon.com/s/ref=nb_sb_noss_2?page=$d&url=search-alias%3Daps&field-keywords=" . str_replace( ' ', '+', trim($keyword->value) ) );
				if(($page['errno'] != 0) || ($page['http_code'] != 200) || (!$page['content'])){
					$this->error($keyword->value, $page['errno'], $page['http_code']);
					$error = true;
					continue;
				}

				// Create a DOM object
				$html = new simple_html_dom();

				preg_replace( "/\r|\n/", "", $page['content'] );

				// Load HTML from a string
				$html->load($page['content']);

				if(strpos($page['content'], 'Sorry, we just need to make sure you\'re not a robot. For best results, please make sure your browser is accepting cookies.')){
					$this->error($keyword->value, 'bot_protection', $d);
					$error = true;
					continue;
				}

				// Select table of search results of amazon
				$html = $html->getElementById( 'resultsCol' );

				// Each element of page
				$li_arr = $html->find( 'li.s-result-item' );
				for ($l=0; $l<count($li_arr); $l++) {
					$li = $li_arr[$l];

					if ( $html = $li->find( '.a-col-right', 0 ) ) {

						// Resume ads
						if($html->find('h5.s-sponsored-list-header')) continue;

						$position ++;

						// Save product position if exists
						if(isset($li->attr['data-asin'])){
							if ($li->attr['data-asin'] == $this->ASIN){
								$added = true;
								$this->log($keyword->id, $position, $d);
								break;
							}
						}
					}
				}
			}

			if(!$added){
				$this->log($keyword->id, 0, 0, $error ? 'error' : 'success');
			}
		}

		$this->avg();
		return true;
	}

	function track($phrases){
		$Thread = new Thread();

		$Data = new Data();
		$keywords = [];

		foreach ($phrases as $keyword){
			$keyword = trim($keyword);
			if($keyword){
				$id = $Data->table('statistics')->insert([
					'asin'=>$this->ASIN,
					'keyword'=>$keyword,
					'tag'=>$this->tag,
					'type'=>$this->type
				]);

				array_push($keywords, [
					'id' => $id,
					'value' => $keyword
				]);
			}
		}

		$t = $this;
		$all_steps = ceil((count($keywords)-1) / $this::$stream_length);
		if(!$all_steps && count($keywords)) $all_steps = 1;

		for($step=0; $step<$all_steps; $step++) {
			$processing_data = [];

			for($p=0; $p<$this::$stream_length; $p++){
				if(!count($keywords)) break;
				array_push($processing_data, array_shift($keywords));
			}

			$Thread->Create(function () use ($t, $processing_data){
				try{
					$t->check($processing_data);
				}catch (Exception $e){
					$t->error('*', $e->getMessage(), 0);
				}
			});
		}

		$Thread->Run();

		return true;

	}

}